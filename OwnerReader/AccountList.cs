﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OwnerReader
{
	public partial class AccountList : Form
	{
		public DateTime dato;
		public AccountList()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void AccountList_Load(object sender, EventArgs e)
		{
			lblDate.Text = dato.ToString("dd/MM/yyyy");
		
			var sqlString = " with cte as (select distinct o.tag, c.fullcompanyname, o.cvrnumber, o.contextRef, o.value, os.lockeduntil,  o.ishandled, o.pdfAddress from OwnerRelations o " +
							" inner join coredata..company c on c.nationaluniqueidentifier = o.cvrNumber  inner join coredata..Ownership os on os.ownershipcompanyid = c.companyid " +
							" and os.lockeduntil is null  where publicdate = '" + dato.ToString("yyyy-MM-dd") + "') " +
							" select cvrNumber,  case when a.ishandled = 1 then 'ja' else 'nej' end as 'er checket' from cte a  " +
							" inner join xbrl..OwnerRelationCountry ct on lower(a.value) LIKE '%[^a-z]' + ct.CountryName + '[^a-z]%' where a.tag = 'DisclosureOfInvestments' " +
							" union  select a.cvrNumber,  case when a.ishandled = 1 then 'ja' else 'nej' end as 'er checket' from cte a  " +
							" inner join xbrl..OwnerRelationCountry ct on lower(a.value) = ct.CountryName  left outer join cte b on a.cvrNumber = b.cvrNumber " +
							" and a.contextRef = b.contextref and b.tag = 'RelatedEntityName'  where a.tag = 'RelatedEntityRegisteredOffice' " +
							" union  select a.cvrNumber,  case when a.ishandled = 1 then 'ja' else 'nej' end as 'er checket' from cte a  " +
							" inner join xbrl..OwnerRelationCountry ct on lower(a.value) LIKE '%' + ct.CountryName + '%'  " +
							" left outer join cte b on a.cvrNumber = b.cvrNumber and a.contextRef = b.contextref and b.tag = 'RelatedEntityName' " +
							" where a.tag = 'RelatedEntityRegisteredOffice' " +
							" union " +
							" select distinct xo.cvrnumber,  case when xo.ishandled = 1 then 'ja' else 'nej' end as 'er checket' from coredata..ownership o  inner join coredata..company cm on cm.companyid = o.OwnershipCompanyID " +
							" inner join coredata..company cc " +
							" on cc.companyid = o.companyid  inner join coredata..Country_Text ct on ct.LanguageID = 208 and ct.CountryID = cc.CountryID " +
							" inner join xbrl..OwnerRelations xo on xo.cvrnumber = cm.NationalUniqueIdentifier  where LockedUntil is null and cc.CountryID <> 208 " +
							" and xo.publicdate = '" + dato.ToString("yyyy-MM-dd") + "' order by cvrnumber ";
			var dt = logic.getData(sqlString);
			dgAccountList.DataSource = dt;
			lblCnt.Text = dt.Rows.Count.ToString();
		}

		private void dgAccountList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridView gridView = sender as DataGridView;
			if (null != gridView)
			{
				foreach (DataGridViewRow r in gridView.Rows)
				{
					gridView.Rows[r.Index].HeaderCell.Value = (r.Index + 1).ToString();
				}
			}
		}
	}	
}
