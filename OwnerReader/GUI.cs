﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OwnerReader
{
	public partial class GUI : Form
	{
		List<OwnerElement> ownerList = new List<OwnerElement>();
		List<prevOwnerElement> prevOwnerList = new List<prevOwnerElement>();
		List<Int64> cvrNumbers = new List<Int64>();
		Int64 currCvr = 0;
		string currPdf = string.Empty;
					
		/*******************************************************************************/
		public GUI()
		{
			InitializeComponent();
			dtPublicDate.Value = DateTime.Today.AddDays(-1);
			txtLatestCvr.Text = ConfigurationManager.AppSettings["sidsteCvr"];
		}

		/*****************************************************************************/
		private void btnFetch_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			if (chkForeign.Checked)
			{
				var sqlPrevOwner = " select distinct xo.pdfAddress, xo.cvrnumber, cm.FullCompanyName as parentCompany, cc.fullcompanyname as childCompany, ct.Country, o.ownershipShare, xo.isHandled from coredata..ownership o " +
								   " inner join coredata..company cm on cm.companyid = o.OwnershipCompanyID " +
								   " inner join coredata..company cc on cc.companyid = o.companyid " +
								   " inner join coredata..Country_Text ct on ct.LanguageID = 208 and ct.CountryID = cc.CountryID " +
								   " inner join xbrl..OwnerRelations xo on xo.cvrnumber = cm.NationalUniqueIdentifier " +
								   " where LockedUntil is null and cc.CountryID <> 208 and xo.publicdate = '" + dtPublicDate.Value.ToString("yyyy-MM-dd") + "' order by xo.cvrnumber ";


				var sqlString = " with cte as (select distinct o.tag, c.fullcompanyname, o.cvrnumber, o.contextRef, o.value, os.lockeduntil,  o.ishandled, o.pdfAddress from OwnerRelations o " +
								" inner join coredata..company c on c.nationaluniqueidentifier = o.cvrNumber " +
								" inner join coredata..Ownership os on os.ownershipcompanyid = c.companyid and os.lockeduntil is null " +
								" where publicdate = '" + dtPublicDate.Value.ToString("yyyy-MM-dd") + "') " +
								" select cvrNumber, tag, value, isHandled, FullCompanyName, pdfAddress, ct.CountryName from cte a " +
								" inner join xbrl..OwnerRelationCountry ct on lower(a.value) LIKE '%[^a-z]' + ct.CountryName + '[^a-z]%' where a.tag = 'DisclosureOfInvestments' " +
								" union " +
								" select a.cvrNumber, a.tag, b.value, a.isHandled, a.FullCompanyName, a.pdfAddress, ct.CountryName from cte a " +
								" inner join xbrl..OwnerRelationCountry ct on lower(a.value) = ct.CountryName " +
								" left outer join cte b on a.cvrNumber = b.cvrNumber and a.contextRef = b.contextref and b.tag = 'RelatedEntityName' " +
								" where a.tag = 'RelatedEntityRegisteredOffice' " +
								" union " +
								" select a.cvrNumber, a.tag, b.value, a.isHandled, a.FullCompanyName, a.pdfAddress, ct.CountryName from cte a " +
								" inner join xbrl..OwnerRelationCountry ct on lower(a.value) LIKE '%' + ct.CountryName + '%' " +
								" left outer join cte b on a.cvrNumber = b.cvrNumber and a.contextRef = b.contextref and b.tag = 'RelatedEntityName' " +
								" where a.tag = 'RelatedEntityRegisteredOffice' order by a.cvrnumber ";

				var dtPrevOwner = logic.getData(sqlPrevOwner);

				prevOwnerList = (from row in dtPrevOwner.AsEnumerable()
								 select new prevOwnerElement
								 {
									 cvrNumber = row.Field<Int64>("cvrNumber"),
									 parentCompany = row.Field<string>("parentCompany"),
									 childCompany = row.Field<string>("childCompany"),
									 PDFPath = row.Field<string>("pdfAddress"),
									 country = row.Field<string>("country"),
									 OwnershipShare = row.Field<string>("ownershipShare"),
									 isHandled = row.Field<bool>("isHandled")
								 }).ToList();

				var dtXbrl = logic.getData(sqlString);

				ownerList = (from row in dtXbrl.AsEnumerable()
							 select new OwnerElement
							 {
								 cvrNumber = row.Field<Int64>("cvrNumber"),
								 tag = row.Field<string>("tag"),
								 value = row.Field<string>("value"),
								 companyName = row.Field<string>("FullCompanyName"),
								 isHandled = row.Field<bool>("isHandled"),
								 PDFPath = row.Field<string>("pdfAddress"),
								 CountryName = row.Field<string>("CountryName")								
							 }).ToList();

				cvrNumbers = ownerList.Where(x => !x.isHandled).Select(x => x.cvrNumber).Concat(prevOwnerList.Where(x => !x.isHandled).Select(x => x.cvrNumber)).OrderBy(x => x).Distinct().ToList<Int64>();
	
				currCvr = cvrNumbers.FirstOrDefault();

				showOwner();
				Cursor.Current = Cursors.Default;
			}
			else
			{
				var sqlString = " select cvrNumber, tag, value, isHandled, c.FullCompanyName, pdfAddress ,'' as CountryName from ownerRelations o inner join coredata..company c on o.cvrnumber = c.NationalUniqueIdentifier " +
								" where publicDate = '" + dtPublicDate.Value.ToString("yyyy-MM-dd") + "'  and c.MainDepartment = 1 and c.StatusID <> 1000  order by cvrnumber";

				var dt = logic.getData(sqlString);

				ownerList = (from row in dt.AsEnumerable()
							 select new OwnerElement
							{
								cvrNumber = row.Field<Int64>("cvrNumber"),
								tag = row.Field<string>("tag"),
								value = row.Field<string>("value"),
								companyName = row.Field<string>("FullCompanyName"),
								isHandled = row.Field<bool>("isHandled"),
								PDFPath = row.Field<string>("pdfAddress")
							}).ToList();

				cvrNumbers = ownerList.Select(x => x.cvrNumber).Distinct().ToList<Int64>();
				currCvr = cvrNumbers.FirstOrDefault();

				showOwner();
			}					
		}

		/**********************************************************************************/
		private void showOwner()
		{
			string xmlNotes = string.Empty;
			txtViewCvrNumber.Text = currCvr.ToString();			
			lblCurrNbr.Text = (cvrNumbers.IndexOf(currCvr) + 1).ToString();
			lblCnt.Text = cvrNumbers.Count.ToString();

			if (ownerList.Count == 0 && prevOwnerList.Count == 0) 
			{
				MessageBox.Show("Der findes ingen firmaer for denne dag!");
			}
			else
			{
				var ownerEntry = ownerList.Where(x => x.cvrNumber == currCvr).FirstOrDefault();
				var prevOwnerEntry = prevOwnerList.Where(x => x.cvrNumber == currCvr);

				if (ownerEntry != null) //If company not exists in xbrl, then we find the cvrnumber from midas
				{
					lblCompanyName.Text = ownerEntry.companyName;
					chkIsChecked.Checked = ownerEntry.isHandled;
					currPdf = ownerEntry.PDFPath;
				}
				else
				{
					lblCompanyName.Text = prevOwnerEntry.FirstOrDefault().parentCompany;
					chkIsChecked.Checked = false;
					currPdf = prevOwnerEntry.FirstOrDefault().PDFPath;
				}				

				if (ownerEntry == null)
				{
					txtOwner.Visible = true;
					browse.Visible = false;
					txtOwner.Text = string.Empty;
					if (prevOwnerEntry.Count() == 0)
					{
						logic.AppendText(txtOwner, "Ingen registrerede datterselskaber", Color.Blue, true);
					}
					else
					{
						logic.AppendText(txtOwner, "Registrerede datterselskaber i Midas: ", Color.Blue, true);
						foreach (var item in prevOwnerEntry)
						{
							logic.AppendText(txtOwner, item.childCompany + " : ", Color.Black, false);
							logic.AppendText(txtOwner, item.country, Color.Red, false);
							logic.AppendText(txtOwner, " (" + item.OwnershipShare + ")", Color.Red, true);
						}
					}
				}
				//var c = ownerList.Where(x => x.cvrNumber == currCvr);
				else if (ownerEntry.value.StartsWith("<") || ownerEntry.value.Contains("<br"))
				{
					txtOwner.Visible = false;
					browse.Visible = true;
					xmlNotes = string.Empty;

					if (chkForeign.Checked)
					{
						if (prevOwnerEntry.Count() == 0)
						{
							xmlNotes += "<font color=blue>" + "Ingen registrerede datterselskaber" + " </font><br/>";							
						}
						else
						{
							xmlNotes += "<font color=blue>" + "Registrerede datterselskaber i Midas" + " </font><br/>";
							foreach (var item in prevOwnerEntry)
							{
								xmlNotes += item.childCompany + " : <font color=red> " + item.country + " (" + item.OwnershipShare + ") </font><br/>";
							}
						}
						xmlNotes += "------------------------------------------------------------------------------------------ <br/>";
					}

					var lastXmlValue = string.Empty;
					var xmlContent = string.Empty;
					foreach (var item in ownerList.Where(x => x.cvrNumber == currCvr))
					{						
						//Don't show dublicate tags
						if (item.value == lastXmlValue)
						{
							xmlContent = "<font color=blue>" + item.tag + "</font> : <font color=red>" + item.CountryName + " </font><br/>" + xmlContent;							
						}
						else
						{
							xmlContent += "<font color=blue>" + item.tag + "</font> : <font color=red>" + item.CountryName + " </font><br/>";
							xmlContent += item.value + " <br/>";
							lastXmlValue = item.value;
						}
					}
					xmlNotes = xmlNotes + xmlContent;

					browse.Navigate("about:blank");
					browse.Document.OpenNew(false);
					browse.Document.Write(xmlNotes);
					browse.Refresh();
				}
				else
				{
					txtOwner.Visible = true;
					browse.Visible = false;
					txtOwner.Text = string.Empty;

					if (chkForeign.Checked)
					{
						if (prevOwnerEntry.Count() == 0)
						{
							logic.AppendText(txtOwner, "Ingen registrerede datterselskaber", Color.Blue, true);
						}
						else
						{
							logic.AppendText(txtOwner, "Registrerede datterselskaber i Midas : ", Color.Blue, true);
							foreach (var item in prevOwnerEntry)
							{
								logic.AppendText(txtOwner, item.childCompany + " : ", Color.Black, false);								
								logic.AppendText(txtOwner, "", Color.Red, false);
								logic.AppendText(txtOwner, item.country, Color.Red, false);
								logic.AppendText(txtOwner, " (" + item.OwnershipShare + ")", Color.Red, true);
							}
						}
					}

					logic.AppendText(txtOwner, "-------------------------------------------------------------", Color.Black, true);

					foreach (var item in ownerList.Where(x => x.cvrNumber == currCvr))
					{						
						logic.AppendText(txtOwner, item.tag + " : ", Color.Blue, false);
						logic.AppendText(txtOwner, item.CountryName, Color.Red, true);
						logic.AppendText(txtOwner, item.value, Color.Black, true);						
					}					
				}

				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				//config.AppSettings.Settings["sidsteCvr"].Value = currCvr.ToString();

				config.AppSettings.Settings.Remove("sidsteCvr");
				config.AppSettings.Settings.Add("sidsteCvr", currCvr.ToString());

				config.Save(ConfigurationSaveMode.Modified);
				ConfigurationManager.RefreshSection("appSettings");

				txtLatestCvr.Text = ConfigurationManager.AppSettings["sidsteCvr"];
			}
		}		

		/*****************************************************************************/
		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (cvrNumbers.Count == 0)
				return;

			var x = cvrNumbers.OrderByDescending(i => i).SkipWhile(i => i != currCvr).Skip(1).FirstOrDefault();
			if (x != 0) currCvr = x;
			showOwner();
		}

		/******************************************************************************/
		private void btnNext_Click(object sender, EventArgs e)
		{
			if (cvrNumbers.Count == 0)
				return;

			var x = cvrNumbers.SkipWhile(i => i != currCvr).Skip(1).FirstOrDefault();
			if (x != 0) currCvr = x;
			showOwner();
		}

		/*************************************************************************/
		private void btnCvrNumber_Click(object sender, EventArgs e)
		{
			if (txtCvrNumber.Text == string.Empty)
				return;

			var sqlString = " select cvrNumber, tag, value, isHandled, c.FullCompanyName, pdfAddress from ownerRelations o inner join coredata..company c on o.cvrnumber = c.NationalUniqueIdentifier " +
							" where publicDate = (select distinct publicDate from OwnerRelations where cvrnumber = " + txtCvrNumber.Text + ") and c.MainDepartment = 1 and c.StatusID <> 1000  order by cvrnumber";
			var dt = logic.getData(sqlString);

			ownerList = (from row in dt.AsEnumerable()
						 select new OwnerElement
						{
							cvrNumber = row.Field<Int64>("cvrNumber"),
							tag = row.Field<string>("tag"),
							value = row.Field<string>("value"),
							companyName = row.Field<string>("FullCompanyName"),
							isHandled = row.Field<bool>("isHandled"),
							PDFPath = row.Field<string>("pdfAddress")
						}).ToList();

			cvrNumbers = ownerList.Select(x => x.cvrNumber).Distinct().ToList<Int64>();
			currCvr = Convert.ToInt64(txtCvrNumber.Text);

			showOwner();		
		}

		/*************************************************************************/
		class OwnerElement
		{
			public Int64 cvrNumber { get; set; }
			public string tag { get; set; }
			public string value { get; set; }
			public string companyName { get; set; }
			public bool isHandled { get; set; }
			public string PDFPath { get; set; }
			public string CountryName { get; set; }			
		}

		class prevOwnerElement
		{
			public Int64 cvrNumber { get; set; }
			public string parentCompany { get; set; }
			public string childCompany { get; set; }
			public string country { get; set; }
			public string PDFPath { get; set; }
			public string OwnershipShare { get; set; }
			public bool isHandled { get; set; }
		}

		private void chkIsChecked_CheckedChanged(object sender, EventArgs e)
		{
			if (cvrNumbers.Count == 0 || chkIsChecked.Checked == false)
				return;

			logic.insertData("update ownerRelations set isHandled = 1 where cvrnumber = " + currCvr);
			var x = cvrNumbers.SkipWhile(i => i != currCvr).Skip(1).FirstOrDefault();
			if (x != 0) currCvr = x;
			showOwner();
		}

		private void btnDailyXbrl_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			AccountList acc = new AccountList();
			acc.dato = dtPublicDate.Value;
			acc.Show();

			Cursor.Current = Cursors.Default;
		}

		private void btnPdf_Click(object sender, EventArgs e)
		{
			string path = currPdf;
			if (path != null)
			{
				System.Diagnostics.Process.Start(path);
			}
			else
			{
				MessageBox.Show("programmet kan ikke finde PDF'en");
			}
		}

		private void btnAddCountry_Click(object sender, EventArgs e)
		{
			logic.insertData("insert into OwnerRelationCountry (CountryName) values( '" +	txtNewName.Text + "')");
			if (!string.IsNullOrEmpty(txtNewName.Text))
				MessageBox.Show(txtNewName.Text + " er blevet tilføjet til listen af søgeord!");
			txtNewName.Text = "";
		}		
	}
}
