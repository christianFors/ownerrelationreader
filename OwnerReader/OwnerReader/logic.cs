﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace OwnerReader
{
    public static class logic
    {
        public static string connStrXBRL = ConfigurationManager.ConnectionStrings["XBRL"].ConnectionString;
		public static string logFolder = ConfigurationManager.AppSettings["logFolder"];		
        /*********************************************************/
        public static int insertData(string sqlString)
        {
            SqlConnection conn = new SqlConnection(connStrXBRL);
            SqlCommand command = new SqlCommand(sqlString, conn);
            command.CommandTimeout = 72000;

            using (conn)
            {
                using (command)
                {
                    conn.Open();
                    return command.ExecuteNonQuery();
                }
            }
        }


        /**********************************************************/
        public static int executeStoredProcedure(string procName)
        {
            using (var conn = new SqlConnection(connStrXBRL))
            {
                using (var command = new SqlCommand(procName, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    {
                        conn.Open();
                        command.CommandTimeout = 600;
                        return command.ExecuteNonQuery();
                    }
                }
            }
        }


        /*********************************************************/
        public static int insertParmData(string sqlString, Int64 value)
        {
            using (var conn = new SqlConnection(connStrXBRL))
            {
                using (var command = new SqlCommand(sqlString, conn))
                {
                    conn.Open();
                    command.Parameters.Add("@Value", SqlDbType.BigInt);
                    command.Parameters["@Value"].Value = value;
                    return command.ExecuteNonQuery();
                }
            }
        }


        /*********************************************************/
        public static int insertParmData(string sqlString, string value)
        {
            using (var conn = new SqlConnection(connStrXBRL))
            {
                using (var command = new SqlCommand(sqlString, conn))
                {
					try
					{
						conn.Open();
						command.Parameters.Add("@Value", SqlDbType.VarChar);
						command.Parameters["@Value"].Value = value;
						return command.ExecuteNonQuery();
					}
					catch (Exception ex)
					{
						logic.logEvent(ex.Message + " value: " + value + " sqlexpression: " + sqlString, DateTime.Now);						
						return -1;
					}
                }
            }
        }


        /*********************************************************/
        public static DataTable getData(string sqlString)
        {

            DataTable dtOutput = new DataTable();

            using (var conn = new SqlConnection(connStrXBRL))
            {
                using (var command = new SqlCommand(sqlString, conn))
                {
					command.CommandTimeout = 0;
                    using (var daAdapt = new SqlDataAdapter(command))
                    {
                        conn.Open();
                        daAdapt.Fill(dtOutput);
                    }
                }
            }
            return dtOutput;
        }


        /*********************************************************/
        public static void truncate(string tblName)
        {
            string sqlString = "truncate table " + tblName;
            insertData(sqlString);
        }


        /*********************************************************/
        public static string getScalar(string sqlString)
        {
            using (var conn = new SqlConnection(connStrXBRL))
            {
                using (var command = new SqlCommand(sqlString, conn))
                {
                    conn.Open();
                    return command.ExecuteScalar().ToString();
                }
            }
        }

        /*********************************************************/
        public static void bulkInsertEnum(List<elemHierarchy> myEnum)
        {
            using (var connection = new SqlConnection(connStrXBRL))
            {
                connection.Open();
                //using (SqlTransaction transaction = connection.BeginTransaction())
                //{
                    using (var bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.BatchSize = 500;
                        bulkCopy.DestinationTableName = "taxanomy";
                        bulkCopy.WriteToServer(myEnum.AsDataTable());                       
                    }
                //}               
            }
        }
				/*************************************************************/
		public static void logEvent(string logText, DateTime startTime)
		{			
			File.AppendAllText(logFolder, (DateTime.Now - startTime).ToString() + " - " + logText + " Time: " + DateTime.Now + Environment.NewLine);			
		}

		public static void AppendText(this RichTextBox box, string text, Color color, bool AddNewLine = false)
		{
			if (AddNewLine)
			{
				text += Environment.NewLine;
			}

			box.SelectionStart = box.TextLength;
			box.SelectionLength = 0;

			box.SelectionColor = color;
			box.AppendText(text);
			box.SelectionColor = box.ForeColor;
		}

    }

        /************************************************************/
        public static class IEnumerableExtensions
        {
            public static DataTable AsDataTable<T>(this IEnumerable<T> data)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                var table = new DataTable();
                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
                return table;
            }
        }

        public class elemHierarchy
        {
            public string name { get; set; }
            public string parent { get; set; }           
            public double displayOrder { get; set; }
            public string taxanomyName { get; set; }
            public int lvl { get; set; }            
        }

		public class pdfInfo
		{
			public int cvrnumber { get; set; }
			public string pdfAddress { get; set; }
			public DateTime startDate { get; set; }
			public DateTime endDate { get; set; }
		}
}
