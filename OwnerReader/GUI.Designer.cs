﻿namespace OwnerReader
{
	partial class GUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtNewName = new System.Windows.Forms.TextBox();
			this.btnAddCountry = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.chkForeign = new System.Windows.Forms.CheckBox();
			this.btnPdf = new System.Windows.Forms.Button();
			this.txtLatestCvr = new System.Windows.Forms.TextBox();
			this.txtViewCvrNumber = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnDailyXbrl = new System.Windows.Forms.Button();
			this.chkIsChecked = new System.Windows.Forms.CheckBox();
			this.lblCnt = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lblCurrNbr = new System.Windows.Forms.Label();
			this.lblCompanyName = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnCvrNumber = new System.Windows.Forms.Button();
			this.btnFetch = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnPrev = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtCvrNumber = new System.Windows.Forms.TextBox();
			this.dtPublicDate = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.txtOwner = new System.Windows.Forms.RichTextBox();
			this.browse = new System.Windows.Forms.WebBrowser();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.IsSplitterFixed = true;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
			this.splitContainer1.Panel1.Controls.Add(this.label6);
			this.splitContainer1.Panel1.Controls.Add(this.chkForeign);
			this.splitContainer1.Panel1.Controls.Add(this.btnPdf);
			this.splitContainer1.Panel1.Controls.Add(this.txtLatestCvr);
			this.splitContainer1.Panel1.Controls.Add(this.txtViewCvrNumber);
			this.splitContainer1.Panel1.Controls.Add(this.label4);
			this.splitContainer1.Panel1.Controls.Add(this.btnDailyXbrl);
			this.splitContainer1.Panel1.Controls.Add(this.chkIsChecked);
			this.splitContainer1.Panel1.Controls.Add(this.lblCnt);
			this.splitContainer1.Panel1.Controls.Add(this.label5);
			this.splitContainer1.Panel1.Controls.Add(this.lblCurrNbr);
			this.splitContainer1.Panel1.Controls.Add(this.lblCompanyName);
			this.splitContainer1.Panel1.Controls.Add(this.label3);
			this.splitContainer1.Panel1.Controls.Add(this.btnCvrNumber);
			this.splitContainer1.Panel1.Controls.Add(this.btnFetch);
			this.splitContainer1.Panel1.Controls.Add(this.btnNext);
			this.splitContainer1.Panel1.Controls.Add(this.btnPrev);
			this.splitContainer1.Panel1.Controls.Add(this.label2);
			this.splitContainer1.Panel1.Controls.Add(this.txtCvrNumber);
			this.splitContainer1.Panel1.Controls.Add(this.dtPublicDate);
			this.splitContainer1.Panel1.Controls.Add(this.label1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.txtOwner);
			this.splitContainer1.Panel2.Controls.Add(this.browse);
			this.splitContainer1.Size = new System.Drawing.Size(1209, 676);
			this.splitContainer1.SplitterDistance = 110;
			this.splitContainer1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.txtNewName);
			this.groupBox1.Controls.Add(this.btnAddCountry);
			this.groupBox1.Location = new System.Drawing.Point(997, 23);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 84);
			this.groupBox1.TabIndex = 25;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Tilføj søgeord";
			// 
			// txtNewName
			// 
			this.txtNewName.Location = new System.Drawing.Point(20, 53);
			this.txtNewName.Name = "txtNewName";
			this.txtNewName.Size = new System.Drawing.Size(161, 20);
			this.txtNewName.TabIndex = 25;
			// 
			// btnAddCountry
			// 
			this.btnAddCountry.Location = new System.Drawing.Point(20, 18);
			this.btnAddCountry.Name = "btnAddCountry";
			this.btnAddCountry.Size = new System.Drawing.Size(161, 23);
			this.btnAddCountry.TabIndex = 24;
			this.btnAddCountry.Text = "Tilføj";
			this.btnAddCountry.UseVisualStyleBackColor = true;
			this.btnAddCountry.Click += new System.EventHandler(this.btnAddCountry_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(801, 45);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(91, 13);
			this.label6.TabIndex = 23;
			this.label6.Text = "Nummer på listen:";
			// 
			// chkForeign
			// 
			this.chkForeign.AutoSize = true;
			this.chkForeign.Checked = true;
			this.chkForeign.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkForeign.Location = new System.Drawing.Point(660, 75);
			this.chkForeign.Name = "chkForeign";
			this.chkForeign.Size = new System.Drawing.Size(122, 17);
			this.chkForeign.TabIndex = 22;
			this.chkForeign.Text = "Vis kun udenlandsk:";
			this.chkForeign.UseVisualStyleBackColor = true;
			// 
			// btnPdf
			// 
			this.btnPdf.Location = new System.Drawing.Point(525, 76);
			this.btnPdf.Name = "btnPdf";
			this.btnPdf.Size = new System.Drawing.Size(119, 23);
			this.btnPdf.TabIndex = 21;
			this.btnPdf.Text = "Hent pdf";
			this.btnPdf.UseVisualStyleBackColor = true;
			this.btnPdf.Click += new System.EventHandler(this.btnPdf_Click);
			// 
			// txtLatestCvr
			// 
			this.txtLatestCvr.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtLatestCvr.Location = new System.Drawing.Point(912, 76);
			this.txtLatestCvr.Name = "txtLatestCvr";
			this.txtLatestCvr.ReadOnly = true;
			this.txtLatestCvr.Size = new System.Drawing.Size(72, 13);
			this.txtLatestCvr.TabIndex = 20;
			// 
			// txtViewCvrNumber
			// 
			this.txtViewCvrNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtViewCvrNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtViewCvrNumber.Location = new System.Drawing.Point(154, 9);
			this.txtViewCvrNumber.Name = "txtViewCvrNumber";
			this.txtViewCvrNumber.ReadOnly = true;
			this.txtViewCvrNumber.Size = new System.Drawing.Size(100, 22);
			this.txtViewCvrNumber.TabIndex = 19;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(801, 76);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(105, 13);
			this.label4.TabIndex = 17;
			this.label4.Text = "Seneste Cvrnummer:";
			// 
			// btnDailyXbrl
			// 
			this.btnDailyXbrl.Location = new System.Drawing.Point(388, 76);
			this.btnDailyXbrl.Name = "btnDailyXbrl";
			this.btnDailyXbrl.Size = new System.Drawing.Size(129, 23);
			this.btnDailyXbrl.TabIndex = 16;
			this.btnDailyXbrl.Text = "Se dagens regnskaber:";
			this.btnDailyXbrl.UseVisualStyleBackColor = true;
			this.btnDailyXbrl.Click += new System.EventHandler(this.btnDailyXbrl_Click);
			// 
			// chkIsChecked
			// 
			this.chkIsChecked.AutoSize = true;
			this.chkIsChecked.Location = new System.Drawing.Point(660, 45);
			this.chkIsChecked.Name = "chkIsChecked";
			this.chkIsChecked.Size = new System.Drawing.Size(81, 17);
			this.chkIsChecked.TabIndex = 15;
			this.chkIsChecked.Text = "Er checket:";
			this.chkIsChecked.UseVisualStyleBackColor = true;
			this.chkIsChecked.CheckedChanged += new System.EventHandler(this.chkIsChecked_CheckedChanged);
			// 
			// lblCnt
			// 
			this.lblCnt.AutoSize = true;
			this.lblCnt.Location = new System.Drawing.Point(947, 45);
			this.lblCnt.Name = "lblCnt";
			this.lblCnt.Size = new System.Drawing.Size(13, 13);
			this.lblCnt.TabIndex = 14;
			this.lblCnt.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(927, 45);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(12, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "/";
			// 
			// lblCurrNbr
			// 
			this.lblCurrNbr.AutoSize = true;
			this.lblCurrNbr.Location = new System.Drawing.Point(907, 45);
			this.lblCurrNbr.Name = "lblCurrNbr";
			this.lblCurrNbr.Size = new System.Drawing.Size(13, 13);
			this.lblCurrNbr.TabIndex = 12;
			this.lblCurrNbr.Text = "0";
			// 
			// lblCompanyName
			// 
			this.lblCompanyName.AutoSize = true;
			this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCompanyName.Location = new System.Drawing.Point(337, 9);
			this.lblCompanyName.Name = "lblCompanyName";
			this.lblCompanyName.Size = new System.Drawing.Size(0, 24);
			this.lblCompanyName.TabIndex = 11;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 82);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Vælg Cvrnummer:";
			// 
			// btnCvrNumber
			// 
			this.btnCvrNumber.Location = new System.Drawing.Point(307, 76);
			this.btnCvrNumber.Name = "btnCvrNumber";
			this.btnCvrNumber.Size = new System.Drawing.Size(75, 23);
			this.btnCvrNumber.TabIndex = 9;
			this.btnCvrNumber.Text = "Hent";
			this.btnCvrNumber.UseVisualStyleBackColor = true;
			this.btnCvrNumber.Click += new System.EventHandler(this.btnCvrNumber_Click);
			// 
			// btnFetch
			// 
			this.btnFetch.Location = new System.Drawing.Point(307, 41);
			this.btnFetch.Name = "btnFetch";
			this.btnFetch.Size = new System.Drawing.Size(75, 23);
			this.btnFetch.TabIndex = 8;
			this.btnFetch.Text = "Hent";
			this.btnFetch.UseVisualStyleBackColor = true;
			this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
			// 
			// btnNext
			// 
			this.btnNext.Location = new System.Drawing.Point(525, 41);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(119, 23);
			this.btnNext.TabIndex = 6;
			this.btnNext.Text = "Næste regnskab";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// btnPrev
			// 
			this.btnPrev.Location = new System.Drawing.Point(387, 41);
			this.btnPrev.Name = "btnPrev";
			this.btnPrev.Size = new System.Drawing.Size(130, 23);
			this.btnPrev.TabIndex = 5;
			this.btnPrev.Text = "Forrige regnskab";
			this.btnPrev.UseVisualStyleBackColor = true;
			this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 24);
			this.label2.TabIndex = 3;
			this.label2.Text = "Cvr-nummer:";
			// 
			// txtCvrNumber
			// 
			this.txtCvrNumber.Location = new System.Drawing.Point(154, 79);
			this.txtCvrNumber.Name = "txtCvrNumber";
			this.txtCvrNumber.Size = new System.Drawing.Size(147, 20);
			this.txtCvrNumber.TabIndex = 2;
			// 
			// dtPublicDate
			// 
			this.dtPublicDate.Location = new System.Drawing.Point(154, 45);
			this.dtPublicDate.Name = "dtPublicDate";
			this.dtPublicDate.Size = new System.Drawing.Size(147, 20);
			this.dtPublicDate.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 46);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(135, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Vælg offentliggørelsesdato:";
			// 
			// txtOwner
			// 
			this.txtOwner.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtOwner.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtOwner.Location = new System.Drawing.Point(0, 0);
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.Size = new System.Drawing.Size(1209, 562);
			this.txtOwner.TabIndex = 1;
			this.txtOwner.Text = "";
			this.txtOwner.Visible = false;
			// 
			// browse
			// 
			this.browse.Dock = System.Windows.Forms.DockStyle.Fill;
			this.browse.Location = new System.Drawing.Point(0, 0);
			this.browse.MinimumSize = new System.Drawing.Size(20, 20);
			this.browse.Name = "browse";
			this.browse.Size = new System.Drawing.Size(1209, 562);
			this.browse.TabIndex = 0;
			// 
			// GUI
			// 
			this.AcceptButton = this.btnNext;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1209, 676);
			this.Controls.Add(this.splitContainer1);
			this.Name = "GUI";
			this.Text = "Ejerrelationer";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.DateTimePicker dtPublicDate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Button btnPrev;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtCvrNumber;
		private System.Windows.Forms.Button btnFetch;
		private System.Windows.Forms.WebBrowser browse;
		private System.Windows.Forms.RichTextBox txtOwner;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnCvrNumber;
		private System.Windows.Forms.Label lblCompanyName;
		private System.Windows.Forms.Label lblCnt;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblCurrNbr;
		private System.Windows.Forms.CheckBox chkIsChecked;
		private System.Windows.Forms.Button btnDailyXbrl;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtViewCvrNumber;
		private System.Windows.Forms.TextBox txtLatestCvr;
		private System.Windows.Forms.Button btnPdf;
		private System.Windows.Forms.CheckBox chkForeign;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button btnAddCountry;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtNewName;
	}
}